package app.binar;

import app.binar.models.*;
import app.binar.models.CustomerReq;
import app.binar.models.CustomerRes;
import app.binar.models.SeaRes;
import app.binar.models.PhoneRes;
import app.binar.models.ResellerReg;
import app.binar.models.GunplaRes;
import app.binar.models.MedicineRes;

public class MainClass {

    public static void main(String[] args) {
        CustomerRes customerRes = new CustomerRes("323232", "regie");
        System.out.println(customerRes.getId());
        System.out.println(customerRes.getName());

        CustomerReq customerReq = new CustomerReq("133232e", "regie", "jkt", true);
        System.out.println(customerReq.getId());
        System.out.println(customerReq.getName());
        System.out.println(customerReq.getAddress());
        System.out.println(customerReq.isActive());

        PhoneRes phoneRes = new PhoneRes("iPhone", "14", "max", true);
        System.out.println(phoneRes.getBrand());
        System.out.println(phoneRes.getType());
        System.out.println(phoneRes.getSeries());
        System.out.println(phoneRes.isActive());

        ResellerReg resellerReg = new ResellerReg("1789", "ririn",  "tangerang", true);
        System.out.println(resellerReg.getId());
        System.out.println(resellerReg.getName());
        System.out.println(resellerReg.getAddress());
        System.out.println(resellerReg.isActive());

        ProfileReq profileReq = new ProfileReq ("666666", "najmia", "Bekasi", true);
        System.out.println(profileReq.getId());
        System.out.println(profileReq.getName());
        System.out.println(profileReq.getAddress());
        System.out.println(profileReq.isActive());

        StudentReq studentReq = new StudentReq("123321","oktavia");
        System.out.println(studentReq.getId());
        System.out.println(studentReq.getName());

        GunplaRes gunplaRes = new GunplaRes("0001", "kiki", "jaksel", true);
        System.out.println(gunplaRes.getId());
        System.out.println(gunplaRes.getName());
        System.out.println(gunplaRes.getAddress());
        System.out.println(gunplaRes.isActive());

        SeaRes seaRes = new SeaRes("1122","asiyah","jkt","2",true);
        System.out.println(seaRes.getName());
        System.out.println(seaRes.getLocation());
        System.out.println(seaRes.getTicket());
    }
}

