package app.binar.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class PhoneRes {
    private String brand;
    private String type;
    private String series;
    private boolean isActive;
}
