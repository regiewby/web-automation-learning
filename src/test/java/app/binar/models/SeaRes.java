package app.binar.models;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class SeaRes {
    private String id;
    private String name;
    private String location;
    private String ticket;
    private boolean isActive ;

}
