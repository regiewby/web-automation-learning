package app.binar.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GunplaRes {
    private String id;
    private String name;
    private String address;
    private boolean isActive;
}
