package app.binar.models;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class ProfileReq {
        private String id;
        private String name;
        private String address;
        private boolean isActive;
}
