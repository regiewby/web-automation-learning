package app.binar.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class ResellerReg {
    private String id;
    private String name;
    private String address;
    private boolean isActive;
}
