package app.binar.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerNew {
    private String id;
    private String name;
}
